#!/usr/bin/env python
import re,sys,string
def getwords(text=None):
	return re.split(r" *",text)

"""
if __name__ == '__main__':
	if len(sys.argv) == 2:
		print getwords(sys.argv[1])
"""
#print getwords("This88383 sd9s@@@44$$$ is a test fun")


def cleanwords(text=None):

	#remove punctuation
	text = text.translate(None, string.punctuation)

	#lowercase string
	text = text.lower()

	words_in_lowercase = getwords(text)


	return filter(lambda x:x if not x.isdigit() else None,words_in_lowercase)

#print cleanwords("This88383 44$$$ is a test fun")