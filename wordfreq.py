from getwords_cleanwords import cleanwords

def wordfreq(file_path=None):
	worlds_fileobj = file(file_path,'r')
	word_freq_dict = {}
	for line in worlds_fileobj:

		#call cleanwords and create frequency
		for word in cleanwords(line.replace('\n','')):
			if word_freq_dict.get(word):
				word_freq_dict[word] = word_freq_dict[word] + 1
			else:
				word_freq_dict[word] = 1
	worlds_fileobj.close()
	return word_freq_dict

#print wordfreq('words.txt')


def wordfreq_without_noise_words(file_path=None,noise_words_file_path=None):
	
	#read noise words from file and create set of noise words
	noise_words_fileobj = file(noise_words_file_path,'r')
	noise_words = set()
	for line in noise_words_fileobj:

		#call cleanwords and update noisewords
		noise_words.update(cleanwords(line.replace('\n','')))
	noise_words_fileobj.close()

	worlds_fileobj = file(file_path,'r')
	word_freq_dict = {}
	for line in worlds_fileobj:

		#call cleanwords and create frequency
		for word in cleanwords(line.replace('\n','')):
			if word not in noise_words:
				if word_freq_dict.get(word):
					word_freq_dict[word] = word_freq_dict[word] + 1
				else:
					word_freq_dict[word] = 1
	worlds_fileobj.close()
	return word_freq_dict

#print wordfreq_without_noise_words('words.txt','noisewords.txt')